//
//  Created by doctorset on 7/17/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#ifndef SPRITE_H
#define SPRITE_H

#include <set>

namespace match3 {
    
    class Sprite : public std::enable_shared_from_this<Sprite> {
    public:
        class expiredRenderer : public std::exception {};
        class wrongLoading : public std::exception {};
        
        Sprite();
        Sprite(const std::string& filePath, std::shared_ptr<SDL_Renderer> renderer);
        
        auto setPosition(const Point& pt)->void;
        auto getPosition() const->const Point&;
        auto setContentSize(const Size& sz)->void;
        auto getContentSize() const->const Size&;
        auto getBoundingBox() const->Rect;
        auto setAnchorPoint(const Point& pt)->void;
        auto getGlobalPosition() const->const Point;
        
        virtual auto handleEvent(std::shared_ptr<SDL_Event> event)->void;
        virtual auto update(float dt)->void;
        virtual auto draw()->void;
        
        auto addChild(std::shared_ptr<Sprite> node)->void;
        auto removeFromParent()->void;
        auto getChildren() const->const std::set<std::shared_ptr<Sprite>>&;
        
        auto moveTo(float time, const Point& pos, std::function<void(std::shared_ptr<Sprite>)> endCB=nullptr)->void;
        
    protected:
        auto _getGlobalPosition() const->const Point;
        
        Point                               _position = { 0, 0 };
        Size                                _contentSize = { 0, 0 };
        Point                               _anchorPoint = { 0.5f, 0.5f };
        
        std::shared_ptr<SDL_Texture>        _texture;
        std::shared_ptr<SDL_Renderer>       _renderer;
        
        std::weak_ptr<Sprite>               _parent;
        std::set<std::shared_ptr<Sprite>>   _children;
        
        float                               _elapsed;
        float                               _firstTick;
        float                               _moveTime = -1.f;
        Point                               _positionDelta;
        Point                               _startPosition;
        std::function<void(std::shared_ptr<Sprite>)>  _moveCB = nullptr;
        
    private:
        auto _updatePosition()->void;
        Point                               _globalPosition = { 0, 0 };
    };
}

#endif
