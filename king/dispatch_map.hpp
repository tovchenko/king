
#ifndef _dispatch_map_hpp
#define _dispatch_map_hpp

#include <memory>
#include <map>

template <typename KeyT, typename ValT>
class dispatch_map : public std::map<KeyT, ValT> {
    class dispatcherNotFound : public std::exception {};
    
public:
    auto dispatcher()->ValT;
    auto activateDispather(const KeyT& key)->void;
    
protected:
    KeyT  _activeKey;
};


template <typename KeyT, typename ValT>
auto dispatch_map<KeyT, ValT>::dispatcher()->ValT {
    auto it = this->find(_activeKey);
    if (it == this->end())
        throw std::make_shared<dispatcherNotFound>();
    
    return it->second;
}

template <typename KeyT, typename ValT>
auto dispatch_map<KeyT, ValT>::activateDispather(const KeyT& key)->void {
    auto it = this->find(key);
    if (it == this->end())
        throw std::make_shared<dispatcherNotFound>();
    
    _activeKey = key;
}

#endif // _dispatch_map_hpp
