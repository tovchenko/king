#ifndef _CombinationChecker_H
#define _CombinationChecker_H


namespace match3 {

    enum class Direction : char {
        HORIZONTAL,
        VERTICAL
    };

    template <typename RowIt, typename CellIt>
    auto findNeighbors(RowIt rowBegin, RowIt rowEnd, RowIt rowIt, CellIt cellIt)->set<pair<RowIt, CellIt>> {
        set<pair<RowIt, CellIt>> res;
        
        if ((*cellIt)->item()) {
            if (cellIt != std::prev(rowIt->end())) {
                auto newCellIt = cellIt;
                if ((*++newCellIt)->item())
                    res.insert(make_pair(rowIt, newCellIt));
            }
            if (cellIt != rowIt->begin()) {
                auto newCellIt = cellIt;
                if ((*--newCellIt)->item())
                    res.insert(make_pair(rowIt, newCellIt));
            }
            if (rowIt != rowEnd - 1) {
                auto it = rowIt;
                ++it;
                auto newCellIt = it->begin() + std::distance(rowIt->begin(), cellIt);
                if ((*newCellIt)->item())
                    res.insert(make_pair(it, newCellIt));
            }
            if (rowIt != rowBegin) {
                auto it = rowIt;
                --it;
                auto newCellIt = it->begin() + std::distance(rowIt->begin(), cellIt);
                if ((*newCellIt)->item())
                    res.insert(make_pair(it, newCellIt));
            }
        }
        return res;
    }
    
    template <typename RowIt, typename CellIt>
    auto findCellByPoint(const Point& pt, RowIt rowBegin, RowIt rowEnd, RowIt& outRowIt, CellIt& outCellIt)->bool {
        bool res = false;
        outRowIt = find_if(rowBegin, rowEnd, [&pt, &outCellIt, &res](typename RowIt::reference row) {
            outCellIt = find_if(row.begin(), row.end(), [&pt](typename CellIt::reference item) {
                return item->getBoundingBox().containsPoint(pt);
            });
            res = outCellIt != row.end();
            return res;
        });
        return res;
    }
    
    template <typename RowIt, typename CellIt>
    auto collectItems(RowIt beginIt, RowIt endIt,
                      RowIt rowIt, CellIt cellIt,
                      Direction dir,
                      bool useChecking = true)->set<pair<RowIt, CellIt>>
    {
        set<pair<RowIt, CellIt>> res;
        auto startItem = (*cellIt)->item();
        if (!startItem) return res;
        
        if (Direction::HORIZONTAL == dir) {
            if (cellIt != std::prev(rowIt->end())) {
                for (auto it = cellIt + 1; it != rowIt->end(); ++it) {
                    if ((*it)->item() && (useChecking ? (*cellIt)->item()->isCompatible(*(*it)->item().get()) : true)) {
                        res.insert(make_pair(rowIt, it));
                    } else {
                        break;
                    }
                }
            }
            if (cellIt != rowIt->begin()) {
                for (auto it = cellIt - 1; it != std::prev(rowIt->begin()); --it) {
                    if ((*it)->item() && (useChecking ? (*cellIt)->item()->isCompatible(*(*it)->item().get()) : true)) {
                        res.insert(make_pair(rowIt, it));
                    } else {
                        break;
                    }
                }
            }
        } else if (Direction::VERTICAL == dir) {
            auto dist = std::distance(rowIt->begin(), cellIt);
            if (rowIt != endIt - 1) {
                for (auto rt = rowIt + 1; rt != endIt; ++rt) {
                    auto it = rt->begin() + dist;
                    if ((*it)->item() && (useChecking ? (*cellIt)->item()->isCompatible(*(*it)->item().get()) : true)) {
                        res.insert(make_pair(rt, it));
                    } else {
                        break;
                    }
                }
            }
            if (rowIt != beginIt) {
                for (auto rt = rowIt - 1; rt != std::prev(beginIt); --rt) {
                    auto it = rt->begin() + dist;
                    if ((*it)->item() && (useChecking ? (*cellIt)->item()->isCompatible(*(*it)->item().get()) : true)) {
                        res.insert(make_pair(rt, it));
                    } else {
                        break;
                    }
                }
            }
        }
        
        return res;
    }
    
    template <typename RowIt, typename CellIt>
    auto checkItems(RowIt beginIt, RowIt endIt, const set<pair<RowIt, CellIt>>& moved)->bool {
        set<shared_ptr<GridCell>> collected;
        for (auto cell : moved) {
            auto cells = (*cell.second)->check();
            set_union(collected.begin(), collected.end(), cells.begin(), cells.end(), inserter(collected, collected.begin()));
        }
        
        if (collected.size() > 0) {
            for (auto cell : collected) {
                cell->item()->wasCollected();
            }
            return true;
        }
        return false;
    }
}

#endif
