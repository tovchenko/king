#ifndef  GRID_H
#define  GRID_H

#include "Sprite.h"
#include <vector>

using namespace std;

namespace match3 {
class GridCell;
class GridItem;

    class Grid final : public Sprite {
    friend class GridCell; //_moveActionWillStart / _moveActionFinished / _addChildToBatchNode
    public:
        typedef vector<vector<shared_ptr<GridCell>>> CellArray;
        typedef CellArray::const_iterator RowIter;
        typedef CellArray::value_type::const_iterator CellIter;
        
        class badIndex : public exception {};
        class wrongCellArray : public exception {};
        
        Grid(const CellArray& cells, shared_ptr<GridCell> proto);
        auto init()->void;
                           
        auto cells() const->const CellArray& { return *_cells; }
        
        auto isAnimating() const->bool { return 0 != _executeCnt; }
        auto setEndAnimationHandler(function<void(const set<pair<RowIter, CellIter>>&)> cb)->void { _moveAnimationWasFinished = cb; }
        auto addMoves(const set<pair<RowIter, CellIter>>& moves)->void;
        
    protected:
        // stack of under items, place where item will move, item which will move
        typedef vector<pair<vector<pair<size_t, size_t>>, pair<pair<size_t, size_t>, weak_ptr<GridItem>>>> Stacks;
        
        // cells layouting
        auto _3x3CellMatrix(const pair<size_t, size_t> idxs) const->vector<shared_ptr<GridCell>>;
        auto _cellByIndices(const pair<size_t, size_t> idxs) const->shared_ptr<GridCell>;
        auto _layoutCells()->void;
        // items moving
        auto _findHangItems() const->Stacks;
        auto _findUnderItems(const pair<size_t, size_t>& hangItem) const->vector<pair<size_t, size_t>>;
        auto _stepMove()->void;
        auto _moveItems(const Stacks& hangs, const Stacks& nextHangs, bool animated)->void;
        auto _restoreMoveItems(const Stacks& hangs)->void;
        auto _moveActionWillStart()->void;
        auto _moveActionFinished()->void;
        // add to batch node
        auto _addChildToBatchNode(shared_ptr<GridItem> item)->void;
        
        shared_ptr<CellArray>               _cells;
        shared_ptr<GridCell>                _emptyCellPrototype;
        shared_ptr<Sprite>                  _walls;
        
        set<pair<RowIter, CellIter>>        _movedCache;
        
        size_t                              _executeCnt = 0;
        function<void(const set<pair<RowIter, CellIter>>&)> _moveAnimationWasFinished = nullptr;
    };
}

#endif

