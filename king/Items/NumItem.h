#ifndef  NUMITEM_H
#define  NUMITEM_H

#include "GridItem.h"

namespace match3 {

    class NumItem : public GridItem {
    public:
        class isntOwned : public exception {};
    
        NumItem(weak_ptr<DispatchMap> dm, const string& spriteName, shared_ptr<SDL_Renderer> renderer, size_t num);
        NumItem(const NumItem& r) : NumItem(r._dm, r._spriteName, r._renderer, r._numCombination) {}
        
        friend auto operator==(const NumItem& lhs, const NumItem& rhs)->bool { return lhs._numCombination == rhs._numCombination; }
        friend auto operator!=(const NumItem& lhs, const NumItem& rhs)->bool { return !(lhs == rhs); }
        
        virtual auto wasCollected()->void override;
        virtual auto collect() const->set<shared_ptr<GridCell>> override;
        virtual auto copy() const->shared_ptr<GridItem> override { return make_shared<NumItem>(*this); }

    protected:
        size_t              _numCombination;
    };
}

#endif

