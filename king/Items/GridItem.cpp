#include "GridItem.h"


using namespace match3;


GridItem::GridItem(weak_ptr<DispatchMap> dm, const string& spriteName, shared_ptr<SDL_Renderer> renderer)
: Sprite(spriteName, renderer),
  _dm(dm),
  _spriteName(spriteName)
{
}

auto GridItem::isCompatible(GridItem& other)->bool {
    if (_dm.expired())
        throw make_shared<failDispatcher>();
    return (*_dm.lock()->dispatcher().get())(*this, other);
}
