#include "ItemsFactory.h"
#include "NumItem.h"
#include "DispatchList0.h"


using namespace match3;


ItemsFactory::ItemsFactory(shared_ptr<SDL_Renderer> renderer)
: _renderer(renderer)
{
    _dm = make_shared<GridItem::DispatchMap>();
    _dm->insert(GridItem::DispatchMap::value_type(DispatchList0::ID, DispatchList0::create()));
    _dm->activateDispather(DispatchList0::ID);
}

auto ItemsFactory::num(size_t num) const->shared_ptr<NumItem> {
    return make_shared<NumItem>(_dm, "item_" + to_string(num) + ".png", _renderer, num);
}