
#include "DispatchList0.h"
#include "NumItem.h"

using namespace match3;


auto DispatchList0::create()->shared_ptr<DispatchList0> {
    auto dd = shared_ptr<DispatchList0>(new DispatchList0);
    dd->_fill();
    return dd;
}

auto DispatchList0::_fill()->void {
    add(GridItem::DD::Type<NumItem>(), GridItem::DD::Type<NumItem>(), [](GridItem& lhs, GridItem& rhs) {
        return *static_cast<NumItem*>(&lhs) == *static_cast<NumItem*>(&rhs);
    });
}