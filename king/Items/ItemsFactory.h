#ifndef  ITEMSFACTORY_H
#define  ITEMSFACTORY_H

#include "GridItem.h"

namespace match3 {
class NumItem;

    class ItemsFactory {
    public:
        ItemsFactory(shared_ptr<SDL_Renderer> renderer);
        
        auto num(size_t num) const->shared_ptr<NumItem>;
        
    protected:
        shared_ptr<GridItem::DispatchMap>   _dm;
        shared_ptr<SDL_Renderer>            _renderer;
    };
}

#endif

