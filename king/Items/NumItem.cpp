#include "NumItem.h"
#include "GridCell.h"
#include "CombinationChecker.h"


using namespace match3;

const size_t kMatchCnt = 2;


NumItem::NumItem(weak_ptr<DispatchMap> dm, const string& spriteName, shared_ptr<SDL_Renderer> renderer, size_t num)
: GridItem(dm, spriteName, renderer),
  _numCombination(num)
{
}

auto NumItem::collect() const->set<shared_ptr<GridCell>> {
    if (_owner.expired())
        throw make_shared<isntOwned>();
        
    auto cell = _owner.lock();
    if (cell->owner().expired())
        throw make_shared<isntOwned>();
        
    set<shared_ptr<GridCell>> res;
        
    auto grid = cell->owner().lock();
    auto cellsH = collectItems(grid->cells().begin(), grid->cells().end(),
                               cell->posInGrid().first, cell->posInGrid().second,
                               Direction::HORIZONTAL);
        
    auto cellsV = collectItems(grid->cells().begin(), grid->cells().end(),
                               cell->posInGrid().first, cell->posInGrid().second,
                               Direction::VERTICAL);
    
    bool collected = false;
    if (cellsH.size() >= kMatchCnt) {
        collected = true;
        for (auto it : cellsH)
            res.insert(*it.second);
    }
    if (cellsV.size() >= kMatchCnt) {
        collected = true;
        for (auto it : cellsV)
            res.insert(*it.second);
    }
    if (collected) {
        res.insert(cell);
    }
    
    return res;
}

auto NumItem::wasCollected()->void {
    if (_owner.expired())
        throw make_shared<isntOwned>();
        
    _owner.lock()->setItem(nullptr);
}
