#ifndef  GRIGITEM_H
#define  GRIGITEM_H

#include "double_dispatch.hpp"
#include "dispatch_map.hpp"
#include "Sprite.h"

using namespace std;

namespace match3 {
class GridCell;

    class GridItem : public Sprite {
    public:
        typedef double_dispatch<bool, GridItem> DD;
        typedef dispatch_map<size_t, shared_ptr<DD>> DispatchMap;
        
        class failDispatcher : public exception {};
        
        GridItem(weak_ptr<DispatchMap> dm, const string& spriteName, shared_ptr<SDL_Renderer> renderer);
        
        auto setOwner(weak_ptr<GridCell> owner)->void { _owner = owner; }
        auto owner() const->weak_ptr<GridCell> { return _owner; }
        
        auto isCompatible(GridItem& other)->bool;
        virtual auto wasCollected()->void {}
        virtual auto collect() const->set<shared_ptr<GridCell>> = 0;
        virtual auto copy() const->shared_ptr<GridItem> = 0;

    protected:
        weak_ptr<DispatchMap>       _dm;
        string                      _spriteName;
        weak_ptr<GridCell>          _owner;
    };
}

#endif

