
#ifndef _match_DispatchList0_
#define _match_DispatchList0_

#include "GridItem.h"

namespace match3 {

    class DispatchList0 final : public GridItem::DD {
    public:
        enum { ID = 0 };
        static auto create()->shared_ptr<DispatchList0>;
        
    protected:
        auto _fill()->void;
    };
}

#endif
