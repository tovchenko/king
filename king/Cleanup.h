//
//  Created by tovchenko on 7/16/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#ifndef CLEANUP_H
#define CLEANUP_H

#include <SDL.h>

namespace match3 {
    
    inline auto cleanup(SDL_Window* win)->void {
        SDL_DestroyWindow(win);
    }
    inline auto cleanup(SDL_Renderer* ren)->void {
        SDL_DestroyRenderer(ren);
    }
    inline auto cleanup(SDL_Texture* tex)->void {
        SDL_DestroyTexture(tex);
    }
    inline auto cleanup(SDL_Surface* surf)->void {
        SDL_FreeSurface(surf);
    }
    
    template <typename T, typename... Args>
    auto cleanup(T* t, Args&&... args)->void {
        cleanup(t);
        cleanup(std::forward<Args>(args)...);
    }
    
    template <typename T>
    auto sdl_shared(T* object)->std::shared_ptr<T> {
        return std::shared_ptr<T>(object, [](T* obj) { cleanup(obj); });
    }
}


#endif
