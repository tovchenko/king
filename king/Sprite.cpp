//
//  GameObject.cpp
//  king
//
//  Created by doctorset on 7/17/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#include "Sprite.h"
#include <SDL.h>
#include <SDL_image.h>

using namespace match3;

Sprite::Sprite() {
}

Sprite::Sprite(const std::string& filePath, std::shared_ptr<SDL_Renderer> renderer) {
    if (!renderer)
        throw std::make_shared<expiredRenderer>();
    
    _renderer = renderer;
    
    _texture = sdl_shared(IMG_LoadTexture(renderer.get(), filePath.c_str()));
    if (!_texture)
        throw std::make_shared<wrongLoading>();
    
    int w, h;
    SDL_QueryTexture(_texture.get(), nullptr, nullptr, &w, &h);
    _contentSize.first = w;
    _contentSize.second = h;
}

auto Sprite::setPosition(const Point& pt)->void {
    _position = pt;
    _updatePosition();
}

auto Sprite::getPosition() const->const Point& {
    return _position;
}

auto Sprite::setContentSize(const Size& sz)->void {
    _contentSize = sz;
}

auto Sprite::getContentSize() const->const Size& {
    return _contentSize;
}

auto Sprite::getBoundingBox() const->Rect {
    Rect r;
    r.x = _position.first - _anchorPoint.first * _contentSize.first;
    r.y = _position.second - _anchorPoint.second * _contentSize.second;
    r.width = _contentSize.first;
    r.height = _contentSize.second;
    
    if (!_parent.expired()) {
        auto size = _parent.lock()->_contentSize;
        auto pt = _parent.lock()->_anchorPoint;
        r.x -= pt.first * size.first;
        r.y -= pt.second * size.second;
    }
    
    return r;
}

auto Sprite::setAnchorPoint(const Point& pt)->void {
    _anchorPoint = pt;
    _updatePosition();
}

auto Sprite::getGlobalPosition() const->const Point {
    auto global = _getGlobalPosition();
    global.first += _anchorPoint.first * getContentSize().first;
    global.second += _anchorPoint.second * getContentSize().second;
    return global;
}

auto Sprite::moveTo(float time, const Point& pos, std::function<void(std::shared_ptr<Sprite>)> endCB)->void {
    _elapsed = 0;
    _firstTick = true;
    _moveTime = time;
    _startPosition = getPosition();
    _positionDelta.first = pos.first - _startPosition.first;
    _positionDelta.second = pos.second - _startPosition.second;
    _moveCB = endCB;
}

auto Sprite::handleEvent(std::shared_ptr<SDL_Event> event)->void {
    auto copyChildren = _children;
    for (auto child : copyChildren)
        child->handleEvent(event);
}

auto Sprite::update(float dt)->void {
    if (_moveTime > 0) {
        if (_firstTick) {
            _firstTick = false;
            _elapsed = 0;
        } else {
            _elapsed += dt;
        }
        
        auto t = _elapsed / _moveTime;
        t = (1 > t ? t : 1);
        t = t > 0 ? t : 0;
        
        float x = _positionDelta.first * t;
        float y = _positionDelta.second * t;
        setPosition(Point(_startPosition.first + x, _startPosition.second + y));
        
        if (_elapsed >= _moveTime) {
            _moveTime = -1.f;
            if (_moveCB)
                _moveCB(shared_from_this());
        }
    }
    
    auto copyChildren = _children;
    for (auto child : copyChildren)
        child->update(dt);
}

auto Sprite::addChild(std::shared_ptr<Sprite> node)->void {
    node->_parent = shared_from_this();
    _children.insert(node);
    node->_updatePosition();
}

auto Sprite::removeFromParent()->void {
    if (_parent.expired())
        return;
    
    auto parent = _parent.lock();
    auto it = parent->_children.find(shared_from_this());
    if (it != parent->_children.end())
        parent->_children.erase(it);
}

auto Sprite::getChildren() const->const std::set<std::shared_ptr<Sprite>>& {
    return _children;
}

auto Sprite::draw()->void {
    if (_texture && _renderer) {
        SDL_Rect dst;
        dst.x = _globalPosition.first;// - _anchorPoint.first * _contentSize.first;
        dst.y = _globalPosition.second;// - _anchorPoint.second * _contentSize.second;
        dst.w = _contentSize.first;
        dst.h = _contentSize.second;
        SDL_RenderCopy(_renderer.get(), _texture.get(), nullptr, &dst);
    }
    
    for (auto child : _children)
        child->draw();
}

auto Sprite::_getGlobalPosition() const->const Point {
    Point res;
    res.first = _position.first - _anchorPoint.first * _contentSize.first;
    res.second = _position.second - _anchorPoint.second * _contentSize.second;
    
    if (!_parent.expired()) {
        auto parentPos = _parent.lock()->_getGlobalPosition();
        res.first += parentPos.first;
        res.second += parentPos.second;
    }
    return res;
}

auto Sprite::_updatePosition()->void {
    _globalPosition = _getGlobalPosition();
    
    for (auto child : _children)
        child->_updatePosition();
}