//
//  Runloop.cpp
//  king
//
//  Created by doctorset on 7/16/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#include "Runloop.h"
#include "Sprite.h"

#include <SDL.h>
#include <SDL_image.h>

using namespace match3;


Runloop::Runloop(const Size& windowSize) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        throw std::make_shared<cantInit>();
    
    if (((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) || ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG))
        throw std::make_shared<cantInit>();
    
    _window = sdl_shared(SDL_CreateWindow("Game", 200, 200, windowSize.first, windowSize.second, SDL_WINDOW_SHOWN));
    if (!_window)
        throw std::make_shared<cantCreateWindow>();
    
    _renderer = sdl_shared(SDL_CreateRenderer(_window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
    if (!_renderer)
        throw std::make_shared<cantCreateRenderer>();
}

Runloop::~Runloop() {
    IMG_Quit();
    SDL_Quit();
}

auto Runloop::run(std::shared_ptr<Sprite> scene)->void {
    _scene = scene;
    
    auto event = std::make_shared<SDL_Event>();
    
    bool quit = false;
    while (!quit) {
        auto curTime = SDL_GetTicks();
        auto delta = (curTime - _ticks) / 1000.0f;
        _ticks = curTime;
        
        while (SDL_PollEvent(event.get())) {
            switch (event->type) {
                case SDL_QUIT:
                    quit = true;
                    break;
            }
            
            _scene->handleEvent(event);
        }
        
        SDL_RenderClear(_renderer.get());
        _scene->update(delta);
        _scene->draw();
        SDL_RenderPresent(_renderer.get());
    }
}

auto Runloop::getContext()->std::shared_ptr<SDL_Renderer> {
    return _renderer;
}