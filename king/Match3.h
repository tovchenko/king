#ifndef MATCH3_H
#define MATCH3_H

#include "Grid.h"


namespace match3 {
class ItemsFactory;

    class Match3 : public Sprite {
    public:
        class gridIsntInit : public exception {};
    
        Match3(shared_ptr<SDL_Renderer> renderer);
        auto init()->void;
        
        virtual auto handleEvent(std::shared_ptr<SDL_Event> event)->void override;
        
    protected:
        auto _gridTouch(const Point& pt)->bool;
    
        shared_ptr<SDL_Renderer>                        _ctx;
        shared_ptr<ItemsFactory>                        _factory;
        shared_ptr<Grid>                                _grid;
        
        // cell touch handling
        Grid::CellArray::const_iterator                 _rowIt;
        Grid::CellArray::value_type::const_iterator     _cellIt;
        set<pair<decltype(_rowIt), decltype(_cellIt)>>  _cellNeighbors;
        bool                                            _touched = false;
        
    private:
        bool                                            _pressed = false;
        bool                                            _cellWasFound = false;
    };
}

#endif
