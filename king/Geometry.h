//
//  Created by doctorset on 7/17/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#ifndef GEOMETRY_H
#define GEOMETRY_H

namespace match3 {
    // All the answers using % here are incorrect, since rand() % n will produce biased results:
    // imagine RAND_MAX == 5 and the number of elements is 4. Then you'll get twice more the number
    // 0 and 1 than the numbers 2 or 3. A correct way to do this is:
    template <typename I>
    auto random_element(I begin, I end)->I {
        auto n = std::distance(begin, end);
        auto divisor = (static_cast<size_t>(RAND_MAX) + 1) / n;
        
        unsigned long k;
        do { k = std::rand() / divisor; } while (k >= n);
        
        auto it = begin;
        std::advance(it, k);
        
        return it;
    }
    
    typedef std::pair<float, float> Point;
    typedef std::pair<float, float> Size;
    
    struct Rect {
        float x;
        float y;
        float width;
        float height;
        
        auto getMaxX() const->float {
            return x + width;
        }
        
        auto getMinX() const->float {
            return x;
        }
        
        auto getMaxY() const->float {
            return y + height;
        }
        
        auto getMinY() const->float {
            return y;
        }
        
        auto containsPoint(const Point& point) const->bool {
            bool ret = false;
            
            if (point.first >= getMinX() && point.first <= getMaxX()
                && point.second >= getMinY() && point.second <= getMaxY())
            {
                ret = true;
            }
            
            return ret;
        }
    };
}

#endif
