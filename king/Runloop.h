//
//  Created by doctorset on 7/16/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#ifndef RUNLOOP_H
#define RUNLOOP_H


class SDL_Window;
class SDL_Renderer;

namespace match3 {
    class Sprite;
    
    class Runloop {
    public:
        class cantInit : public std::exception {};
        class cantCreateWindow : public std::exception {};
        class cantCreateRenderer : public std::exception {};
        
        Runloop(const Size& windowSize);
        virtual ~Runloop();
        
        auto run(std::shared_ptr<Sprite> scene)->void;
        
        auto getContext()->std::shared_ptr<SDL_Renderer>;
        
    protected:
        std::shared_ptr<SDL_Window>     _window;
        std::shared_ptr<SDL_Renderer>   _renderer;
        std::shared_ptr<Sprite>         _scene;
        
        Uint32                          _ticks = 0;
    };
}

#endif