//
// Taras Tovchenko, 2012
//

#if ! defined(DOUBLEDISPATCH_H)
#define DOUBLEDISPATCH_H

#include <list>
#include <algorithm>
#include <memory>
using namespace std::placeholders;
// boost::bind() provides a convenient way to reverse the parameters to a functor.

// double_dispatch is a template because we have to assume that all parameter
// types are subclasses of some common base class -- but we don't have to
// impose that base class on client code.  Instead, we let IT tell US the
// common parameter base class.
template<class ReturnType, class ParamBaseType>
class double_dispatch {
    // This is the base class for each entry in our dispatch table.
    class EntryBase {
    public:
        virtual bool matches(const ParamBaseType& param1, const ParamBaseType& param2) const = 0;
        virtual ReturnType operator()(ParamBaseType& param1, ParamBaseType& param2) const = 0;
    };

    // Here's the template subclass that actually creates each entry.
    template<typename Type1, typename Type2, class Functor>
    class Entry : public EntryBase {
        // Bind whatever function or function object the instantiator passed.
        Functor mFunc;

    public:
        Entry(Functor func) : mFunc(func) {}
        virtual bool matches(const ParamBaseType& param1, const ParamBaseType& param2) const {
            return (dynamic_cast<const Type1*>(&param1) != nullptr &&
                    dynamic_cast<const Type2*>(&param2) != nullptr);
        }
        
        virtual ReturnType operator()(ParamBaseType& param1, ParamBaseType& param2) const {
            return mFunc(dynamic_cast<Type1&>(param1), dynamic_cast<Type2&>(param2));
        }
    };

    typedef std::shared_ptr<EntryBase> EntryPtr;
    typedef std::list<EntryPtr> DispatchTable;
    DispatchTable mDispatch;

    // Look up the first matching entry.
    EntryPtr lookup(const ParamBaseType& param1, const ParamBaseType& param2) const {
        auto found = std::find_if(mDispatch.begin(), mDispatch.end(),
                                  std::bind(&EntryBase::matches, _1, std::ref(param1), std::ref(param2)));
        if (found != mDispatch.end())
            return *found;
        return nullptr;
    }

    // Don't implement the copy ctor.  Everyone will be happier if the
    // DoubleDispatch object isn't copied.
    double_dispatch(const double_dispatch& src);

public:
    double_dispatch() {}

    // Call the first matching entry.  If there's no registered Functor
    // appropriate for this pair of parameter types, this call will do
    // NOTHING!  (If you want notification in this case, simply add a new
    // Functor for (ParamBaseType&, ParamBaseType&) at the end of the table.
    // The two base-class entries should match anything that isn't matched by
    // any more specific entry.)
    ReturnType operator()(ParamBaseType& param1, ParamBaseType& param2) const {
        EntryPtr found = lookup(param1, param2);
        if (found.get() == 0)
            return ReturnType();    // bogus return value

        return (*found)(param1, param2); // otherwise, call the Functor we found
    }

    // Borrow a trick from Alexandrescu:  use a Type class to "wrap" at ype
    // for purposes of passing the type itself into a template method.
    template<typename T>
    struct Type {};

    // Add a new Entry for a given Functor.  As mentioned above, the order in
    // which you add these entries is very important.  If you want symmetrical
    // entries -- that is, if a B and an A can call the same Functor as an A
    // and a B -- then pass 'true' for the last parameter, and we'll add  a (B,
    // A) entry as well as an (A, B) entry.  But your Functor can still be
    // written to expect exactly the pair of types you've explicitly
    // specified, because the entry with the reversed params will call a
    // special functor that swaps params before calling your functor.
    template<typename Type1, typename Type2, class Functor>
    void add(const Type<Type1>& t1, const Type<Type2>& t2, Functor func, bool symmetrical = false) {
        insert(t1, t2, func);
        // The type of boost::bind() is almost certainly NOT the same type as
        // our Functor!  We don't know what type it actually does return --
        // but then, given an appropriate template function, we don't need to know.
        if (symmetrical)
            insert(t2, t1, std::bind(func, _2, _1));
    }

private:
    // Break out the actual insert operation so the public add() template
    // function can avoid calling itself recursively.  See add() comments.
    template<typename Type1, typename Type2, class Functor>
    void insert(const Type<Type1>&, const Type<Type2>&, Functor func) {
        mDispatch.insert(mDispatch.end(), EntryPtr(new Entry<Type1, Type2, Functor>(func)));
    }
};

#endif /* ! defined(DOUBLEDISPATCH_H) */


