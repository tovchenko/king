#include "Grid.h"
#include "GridCell.h"
#include "GridItem.h"


using namespace match3;


Grid::Grid(const CellArray& cells, shared_ptr<GridCell> proto)
: _emptyCellPrototype(proto),
  _cells(make_shared<CellArray>(cells))
{
}

auto Grid::init()->void {
    size_t cnt = 0;
    for (auto row : *_cells) {
        if (row.size() != cnt && 0 != cnt)
            throw make_shared<wrongCellArray>();
            
            cnt = row.size();
            for (auto cell : row) {
                cell->setOwner(static_pointer_cast<Grid>(shared_from_this()));
                
            }
    }
    
    _layoutCells();
    _stepMove();
}

auto Grid::addMoves(const set<pair<RowIter, CellIter>>& moves)->void {
    _movedCache.insert(moves.begin(), moves.end());
}

auto Grid::_3x3CellMatrix(const pair<size_t, size_t> idxs) const->vector<shared_ptr<GridCell>> {
    if (_cells->size() <= idxs.first)
        throw make_shared<badIndex>();
        
    auto row = _cells->at(idxs.second);
    if (row.size() <= idxs.second)
        throw make_shared<badIndex>();
        
    vector<shared_ptr<GridCell>> result;
    
    // 1st
    if (0 == idxs.first) result.push_back(_emptyCellPrototype);
    else if (0 == idxs.second) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first - 1)[idxs.second - 1]);
    // 2nd
    if (0 == idxs.first) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first - 1)[idxs.second]);
    // 3rd
    if (0 == idxs.first) result.push_back(_emptyCellPrototype);
    else if (idxs.second == row.size() - 1) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first - 1)[idxs.second + 1]);
    // 4th
    if (0 == idxs.second) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first)[idxs.second - 1]);
    // 5th
    result.push_back(_cells->at(idxs.first)[idxs.second]);
    // 6th
    if (idxs.second == row.size() - 1) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first)[idxs.second + 1]);
    // 7th
    if (idxs.first == _cells->size() - 1) result.push_back(_emptyCellPrototype);
    else if (0 == idxs.second) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first + 1)[idxs.second - 1]);
    // 8th
    if (idxs.first == _cells->size() - 1) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first + 1)[idxs.second]);
    // 9th
    if (idxs.first == _cells->size() - 1) result.push_back(_emptyCellPrototype);
    else if (idxs.second == row.size() - 1) result.push_back(_emptyCellPrototype);
    else result.push_back(_cells->at(idxs.first + 1)[idxs.second + 1]);
    
    return result;
}

auto Grid::_cellByIndices(const pair<size_t, size_t> idxs) const->shared_ptr<GridCell> {
    if (_cells->size() <= idxs.first)
        return nullptr;
    
    auto row = _cells->at(idxs.first);
    if (row.size() <= idxs.second)
        return nullptr;
    
    return row[idxs.second];
}

auto Grid::_layoutCells()->void {
//    if (_walls) _walls->removeFromParent();
//    _walls = make_shared<Sprite>();
//    addChild(_walls);
    
    auto sz = _emptyCellPrototype->getContentSize();
    
    float h = .5f*sz.second;
    size_t i = 0;
    for (auto rowIt = _cells->begin(); rowIt != _cells->end(); ++rowIt) {
        float w = .5f*sz.first;
        size_t j = 0;
        
        for (auto cellIt = rowIt->begin(); cellIt != rowIt->end(); ++cellIt) {
            (*cellIt)->setPosition(Point(w, h));
            
//            auto drawing = _wallsFactory->wallsByMatrix(_3x3CellMatrix(make_pair(i, j)));
//            for (auto draw : drawing) {
//                draw->setPosition((*cellIt)->getPosition());
//                _walls->addChild(draw);
//            }
            
            (*cellIt)->_setIterators(rowIt, cellIt);
            addChild(*cellIt);
            
            if ((*cellIt)->item()) {
                (*cellIt)->item()->setPosition((*cellIt)->getPosition());
                _addChildToBatchNode((*cellIt)->item());
            }
            
            w += sz.first;
            ++j;
        }
        
        h += sz.second;
        ++i;
    }
    
//    setAnchorPoint(Point(.5f, -.5f));
    setContentSize(Size(sz.first * _cells->begin()->size(),
                        sz.second * _cells->size()));
}

auto Grid::_findHangItems() const->Stacks {
    Stacks res;
    if (_cells->size() <= 1)
        return res;
    
    map<size_t, set<vector<pair<size_t, size_t>>>> stacks;
    const size_t jLast = _cells->begin()->size() - 1;
    
    // starts finding from the bottom of the grid
    size_t i = _cells->size() - 2;
    for (auto rowIt = _cells->rbegin() + 1; rowIt != _cells->rend(); ++rowIt, --i) {
        size_t j = jLast;
        // starts checking from the end because moving to left has higher priority than moving to right
        for (auto it = rowIt->rbegin(); it != rowIt->rend(); ++it, --j) {
            if ((*it)->item()) {
                // checks item
                const auto item = make_pair(i, j);
                // defines all available places
                vector<pair<size_t, size_t>> places({ make_pair(i + 1, j) });
                if (0 != j) places.push_back(make_pair(i + 1, j - 1));
                if (jLast != j) places.push_back(make_pair(i + 1, j + 1));
                // checks these places
                for (auto place : places) {
                    auto cell = _cellByIndices(place);
                    if (cell->canHoldItem() && !cell->item()) {
                        // checks competitors cells
                        if (place.second < item.second || place.second > item.second) {
                            // checks direct move from top
                            auto neigbour = _cellByIndices(make_pair(item.first, place.second));
                            if (neigbour->canHoldItem() && neigbour->item()) {
                                continue;
                            // checks cross move to right
                            } else if (place.second > item.second && place.second < jLast) {
                                if (res.end() != find_if(res.begin(), res.end(), [&place](decltype(res)::reference obj) {
                                    return obj.second.first == place;
                                })) continue;
                            }
                        }
                        // checks whether item already contains by some moved stack
                        bool was = false;
                        auto moved = stacks.find(j);
                        if (moved != stacks.end()) {
                            for (auto stack : moved->second) {
                                if (find(stack.begin(), stack.end(), item) != stack.end()) {
                                    was = true;
                                    break;
                                }
                            }
                        }
                        // if no than we build new moved stack of items
                        if (!was) {
                            auto under = _findUnderItems(item);
                            if (moved != stacks.end()) moved->second.insert(under);
                            else stacks.insert(make_pair(j, set<vector<pair<size_t, size_t>>>({ under })));
                            
                            res.push_back(make_pair(under, make_pair(place, (*it)->item())));
                        }
                    }
                }
            }
        }
    }
    return res;
}

auto Grid::_findUnderItems(const pair<size_t, size_t>& hangItem) const->vector<pair<size_t, size_t>> {
    vector<pair<size_t, size_t>> res;
    if (_cells->size() - 1 == hangItem.first)
        return res;
    
    for (long i = hangItem.first; i >= 0; --i) {
        auto item = _cellByIndices(make_pair(i, hangItem.second));
        if (!item->item())
            break;
        
        res.push_back(make_pair(i, hangItem.second));
    }
    return res;
}

auto Grid::_stepMove()->void {
    auto hangItems = _findHangItems();
    _moveItems(hangItems, Stacks(), false);
    
    auto nextHangItems = _findHangItems();
    _restoreMoveItems(hangItems);
    _moveItems(hangItems, nextHangItems, true);
    
    // send events to cells
    for (auto row : *_cells) for (auto cell : row)
        cell->itemsWereMoved();
}

auto Grid::_moveItems(const Stacks& hangs, const Stacks& nextHangs, bool animated)->void {
    for (auto stack : hangs) {
        auto found = nextHangs.end() != find_if(nextHangs.begin(), nextHangs.end(), [&stack](Stacks::const_reference obj) {
            return obj.second.second.lock() == stack.second.second.lock();
        });
        
        // todo: need to add animation for last move
        auto mode = found ? GridCell::AnimMode::MOVE : GridCell::AnimMode::MOVE;
        auto hangPlace = _cellByIndices(*stack.first.begin());
        auto emptyPlace = _cellByIndices(stack.second.first);
        
        if (animated && !found)
            _movedCache.insert(emptyPlace->posInGrid());
        
        emptyPlace->setItem(hangPlace->item(), animated ? mode : GridCell::AnimMode::NO);
    
        if (stack.first.size() > 1) {
            for (auto it = stack.first.begin() + 1; it != stack.first.end(); ++it) {
                auto hangPlace = _cellByIndices(*it);
                auto emptyPlace = _cellByIndices(*(it - 1));
                
                emptyPlace->setItem(hangPlace->item(), animated ? mode : GridCell::AnimMode::NO);
                if (animated && !found)
                    _movedCache.insert(emptyPlace->posInGrid());
            }
        }
        _cellByIndices(*(std::prev(stack.first.end())))->setItem(nullptr);
    }
}

auto Grid::_restoreMoveItems(const Stacks& hangs)->void {
    for (auto stack : hangs) {
        if (stack.first.size() > 1) {
            for (auto it = stack.first.rbegin() + 1; it != stack.first.rend(); ++it) {
                auto hangPlace = _cellByIndices(*it);
                auto emptyPlace = _cellByIndices(*(it - 1));
                
                emptyPlace->setItem(hangPlace->item(), GridCell::AnimMode::NO);
            }
        }
        
        auto hangPlace = _cellByIndices(*stack.first.begin());
        auto emptyPlace = _cellByIndices(stack.second.first);
        
        hangPlace->setItem(emptyPlace->item(), GridCell::AnimMode::NO);
        emptyPlace->setItem(nullptr);
    }
}

auto Grid::_moveActionWillStart()->void {
    ++_executeCnt;
}

auto Grid::_moveActionFinished()->void {
    if (0 == --_executeCnt) {
        auto hangItems = _findHangItems();
        if (hangItems.size() == 0) {
            if (_moveAnimationWasFinished)
                _moveAnimationWasFinished(_movedCache);
            _movedCache.clear();
        }
        _stepMove();
    }
}

auto Grid::_addChildToBatchNode(shared_ptr<GridItem> item)->void {
    addChild(item);
}