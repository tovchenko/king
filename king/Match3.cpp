#include "Match3.h"
#include "Grid.h"
#include "EmptyCell.h"
#include "CellFactory.h"
#include "ItemsFactory.h"
#include "NumItem.h"
#include "CombinationChecker.h"

using namespace match3;
using namespace std::placeholders;


Match3::Match3(shared_ptr<SDL_Renderer> renderer) {
    _ctx = renderer;
    
    auto cf = make_shared<CellFactory>();
    _factory = make_shared<ItemsFactory>(_ctx);
    
    auto of = _factory;
    vector<shared_ptr<GridCell>> ce;
    for (int i = 0; i < 20; ++i) {
        auto cell = cf->place();
        cell->setItem(i == 0 ? of->num(0) : of->num(2));
        ce.push_back(cell);
    }
    
    vector<shared_ptr<GridCell>> gens;
    for (int i = 0; i < 8; ++i) {
        gens.push_back(cf->generator({of->num(0), of->num(1), of->num(2), of->num(3), of->num(4)}));
    }
    
    Grid::CellArray cells = {
        {gens[0], gens[1], gens[2], gens[3], gens[4], gens[5], gens[6], gens[7]},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()},
        {cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place(), cf->place()}};
    
    _grid = make_shared<Grid>(cells, cf->empty());
    _grid->init();
    _grid->setPosition(Point(120, -30));
    _grid->setEndAnimationHandler([this](const set<pair<Grid::RowIter, Grid::CellIter>>& moved) {
        if (!checkItems(_grid->cells().begin(), _grid->cells().end(), moved) && _touched) {
            auto it = moved.begin();
            auto first = it->second;
            auto second = (++it)->second;
            
            auto tmp = (*first)->item();
            (*first)->setItem((*second)->item());
            (*second)->setItem(tmp);
        }
        
        _touched = false;
    });
}

auto Match3::init()->void {
    addChild(_grid);
}

auto Match3::handleEvent(std::shared_ptr<SDL_Event> event)->void {
    switch (event->type) {
        case SDL_MOUSEBUTTONDOWN: {
            _pressed = true;
            _cellWasFound = false;
            
            auto pos = Point(event->button.x, event->button.y);
            auto pt = _grid->getGlobalPosition();
            pt.first = pos.first - pt.first;
            pt.second = pos.second - pt.second;
            
            if (_gridTouch(pt)) {
                break;
            }
            
            _cellNeighbors.clear();
            _cellWasFound = findCellByPoint(pt, _grid->cells().begin(), _grid->cells().end(), _rowIt, _cellIt);
            if (_cellWasFound) {
                _cellNeighbors = findNeighbors(_grid->cells().begin(), _grid->cells().end(), _rowIt, _cellIt);
            }
            break;
        }
            
        case SDL_MOUSEMOTION: {
            if (_pressed && _cellWasFound) {
                auto pos = Point(event->motion.x, event->motion.y);
                auto pt = _grid->getGlobalPosition();
                pt.first = pos.first - pt.first;
                pt.second = pos.second - pt.second;
                
                _gridTouch(pt);
            }
            break;
        }
            
        case SDL_MOUSEBUTTONUP: {
            _pressed = false;
            _cellWasFound = false;
            break;
        }
    }
}

auto Match3::_gridTouch(const Point& pt)->bool {
    if (_grid->isAnimating())
        return true;
    
    if (0 == _cellNeighbors.size())
        return false;
    
    for (auto cell : _cellNeighbors) {
        if ((*cell.second)->getBoundingBox().containsPoint(pt)) {
            auto tmp = (*cell.second)->item();
            (*cell.second)->setItem((*_cellIt)->item());
            (*_cellIt)->setItem(tmp);
            
            _touched = true;
            _grid->addMoves({make_pair(_rowIt, _cellIt), make_pair(cell.first, cell.second)});
            
            _cellNeighbors.clear();
            return true;
        }
    }
    return false;
}


