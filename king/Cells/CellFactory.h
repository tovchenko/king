#ifndef  CELLFACTORY_H
#define  CELLFACTORY_H

#include <vector>

using namespace std;

namespace match3 {
class GridCell;
class GridItem;

    class CellFactory {
    public:
        CellFactory();
        
        auto empty()->shared_ptr<GridCell>;
        auto place()->shared_ptr<GridCell>;
        auto generator(const vector<shared_ptr<GridItem>>& prototypes)->shared_ptr<GridCell>;
    };
}

#endif

