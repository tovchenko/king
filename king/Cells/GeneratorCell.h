#ifndef  GENERATORCELL_H
#define  GENERATORCELL_H

#include "GridCell.h"

namespace match3 {

    class GeneratorCell : public GridCell {
    public:
        GeneratorCell(const vector<shared_ptr<GridItem>>& prototypes);
        
        virtual auto canHoldItem() const->bool override { return true; }
        virtual auto itemsWereMoved()->void override { _generateItem(); }
        
    protected:
        auto _generateItem()->void;
    
        vector<shared_ptr<GridItem>>    _prototypes;
    };
}

#endif

