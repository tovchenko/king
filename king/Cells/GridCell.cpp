#include "GridCell.h"
#include "GridItem.h"
#include "CombinationChecker.h"


using namespace match3;

const float kDt = .2f;


GridCell::GridCell() {
    setContentSize(Size(40, 40));
}

auto GridCell::setItem(shared_ptr<GridItem> item, AnimMode type)->void {
    if (!canHoldItem()) return;
    
    if (item && !item->owner().expired()) {
        item->owner().lock()->_item = nullptr;
        item->removeFromParent();
        item->setOwner(weak_ptr<GridCell>());
    }
    
    if (_item) {
        _item->removeFromParent();
        _item->setOwner(weak_ptr<GridCell>());
    }
    _item = item;
    if (_item) _item->setOwner(static_pointer_cast<GridCell>(shared_from_this()));
    else return;
    
    if (_owner.expired())
        return;
    
    _owner.lock()->_addChildToBatchNode(_item);
    
    if (AnimMode::NO == type) {
        _item->setPosition(getPosition());
    } else if (AnimMode::MOVE == type) {
        _owner.lock()->_moveActionWillStart();
        _item->moveTo(kDt, getPosition(), [this](std::shared_ptr<Sprite> item) {
            _owner.lock()->_moveActionFinished();
        });
    }
}

auto GridCell::check()->set<shared_ptr<GridCell>> {
    if (_owner.expired())
        throw make_shared<isntOwnedByGrid>();
        
    return _item->collect();
    return set<shared_ptr<GridCell>>();
}
