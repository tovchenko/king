#ifndef  GRIGCELL_H
#define  GRIGCELL_H

#include "Sprite.h"
#include "Grid.h"
using namespace std;

namespace match3 {
class GridItem;

    class GridCell : public Sprite {
    friend class Grid; // _setIterators
    public:
        class isntOwnedByGrid : public exception {};
        
        enum class AnimMode : char {
            NO,
            MOVE
        };
        
        GridCell();
        
        auto setOwner(weak_ptr<Grid> own)->void { _owner = own; }
        auto owner() const->weak_ptr<Grid> { return _owner; }
        auto posInGrid() const->const pair<Grid::RowIter, Grid::CellIter>& { return _gridPosition; }
        
        virtual auto canHoldItem() const->bool { return true; }
        auto setItem(shared_ptr<GridItem> item, AnimMode type=AnimMode::MOVE)->void;
        auto item() const->shared_ptr<GridItem> { return canHoldItem() ? _item : nullptr; }
        
        virtual auto itemsWereMoved()->void {}
        virtual auto check()->set<shared_ptr<GridCell>>;

    protected:
        auto _setIterators(Grid::RowIter rIt, Grid::CellIter cIt)->void { _gridPosition = make_pair(rIt, cIt); }
    
        weak_ptr<Grid>                      _owner;
        pair<Grid::RowIter, Grid::CellIter> _gridPosition;
        shared_ptr<GridItem>                _item;
    };
}

#endif

