#ifndef  EMPTYCELL_H
#define  EMPTYCELL_H

#include "GridCell.h"

namespace match3 {

    class EmptyCell : public GridCell {
    public:
        virtual auto canHoldItem() const->bool override { return false; }
    };
}

#endif

