#include "CellFactory.h"
#include "EmptyCell.h"
#include "GeneratorCell.h"


using namespace match3;


CellFactory::CellFactory() {
}

auto CellFactory::empty()->shared_ptr<GridCell> {
    return make_shared<EmptyCell>();
}

auto CellFactory::place()->shared_ptr<GridCell> {
    return make_shared<GridCell>();
}

auto CellFactory::generator(const vector<shared_ptr<GridItem>>& prototypes)->shared_ptr<GridCell> {
    return make_shared<GeneratorCell>(prototypes);
}
