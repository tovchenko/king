#include "GeneratorCell.h"
#include "GridItem.h"
#include "Grid.h"


using namespace match3;


GeneratorCell::GeneratorCell(const vector<shared_ptr<GridItem>>& prototypes)
: _prototypes(prototypes)
{
}

auto GeneratorCell::_generateItem()->void {
    if (item()) return;
    
    if (_owner.expired())
        throw make_shared<isntOwnedByGrid>();
    
    _owner.lock()->addMoves({posInGrid()});
        
    auto newItem = (*random_element(_prototypes.begin(), _prototypes.end()))->copy();
    newItem->setPosition(Point(getPosition().first, getPosition().second - getContentSize().second));
    setItem(newItem, AnimMode::MOVE);
}