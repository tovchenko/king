//
//  main.m
//  king
//
//  Created by doctorset on 7/16/14.
//  Copyright (c) 2014 tovchenko. All rights reserved.
//

#include "Runloop.h"
#include "Sprite.h"
#include "Match3.h"

const int kScreenWidth = 755;
const int kScreenHeight = 600;

using namespace match3;


int main(int argc, const char* argv[]) {
    try {
        auto loop = std::make_shared<Runloop>(std::make_pair(kScreenWidth, kScreenHeight));
        auto ctx = loop->getContext();
    
        auto scene = std::make_shared<Sprite>("BackGround.jpg", ctx);
        auto game = std::make_shared<Match3>(ctx);
        game->init();
        scene->addChild(game);

        scene->setPosition(Point(0.5f*kScreenWidth, 0.5f*kScreenHeight));
        game->setPosition(Point(0.5f*scene->getContentSize().first, 0.5f*scene->getContentSize().second));
        loop->run(scene);
    } catch (...) {
        std::cout << "Uncaught exception!" << std::endl;
        throw;
    }
    
    return 0;
}
